FROM python:3.7.1-alpine3.8
LABEL maintainer.email="riccardo@codenomads.nl" \
      maintainer.name="Riccardo Mascarenhas" \
      description="This image inherits from python:3.7.1-alpine3.8 and adds Amazon AWS executable (awscli) to it. It is an unofficial image." \
      version="0.0.1"
ENV PATH "$PATH:/root/.local/bin"
RUN pip install awscli --upgrade --user
