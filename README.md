# Amazon CLI - Unofficial Docker image #
Amazon still does not provide an official Docker image, as stated on these issues on their GitHub:
* [Provide Official AWS CLI Docker Image #3291](https://github.com/aws/aws-cli/issues/3291 "Provide Official AWS CLI Docker Image #3291") - \[CLOSED]
* [An official Docker Image with the AWS CLI for use in CI/CD scenarios #3553](https://github.com/aws/aws-cli/issues/3553 "An official Docker Image with the AWS CLI for use in CI/CD scenarios #3553") - \[OPEN]
  * This issue is currently labeled as a "feature-request" with no planned milestone.

This image has the intent to solve this issue while we still wait for the official one.
As `awscli` relies on `python3` to its execution, this image inherits from the official `python:3.7.1-alpine3.8` which is based on Alpine lightweight Linux distro.

---

This project is configured to run on GitLab CI, and is implementing the following behaviors - all based on a git push to remote:

| **Trigger**   | **Registry**      | **Image name**.       | **GitLab CI Jobs**        |
| ---           | ---               | ---                   | ---                       |
| any branch    | GitLab (private)  | `awscli:branch-name`  | build                     |
| master branch | Docker Hub        | `awscli:latest`       | build-master              |
| any tag       | Docker Hub        | `awscli:tag-name`     | build-master, build-tag   |

**PS.:** When CI pipeline was triggered by *"any tag"* both *build-master* and *build-tag* behaviors will be applied, which means that the image will be pushed to Docker Hub registry and two tags will be created for it: `awscli:latest` and `awscli:tag-name`